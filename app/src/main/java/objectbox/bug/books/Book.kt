package objectbox.bug.books

import io.objectbox.annotation.Backlink
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.relation.ToMany
import objectbox.bug.verses.Verse

@Entity
data class Book(
        @Id(assignable = true)
        var id: Long = 0,
        var name: String
) {
    @Backlink
    lateinit var verses: ToMany<Verse>
}