package objectbox.bug

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import objectbox.bug.books.Book
import objectbox.bug.verses.Verse
import java.io.IOException

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        getBooks()
    }

    private fun getBooks(): List<Book> {

        val booksBox = boxStore().boxFor(Book::class.java)
        var books = booksBox.all
        if (books.isEmpty()) {
            books = Gson().fromJson(getJson(this), Array<Book>::class.java).toList()
            booksBox.put(books)
        }
        books.first().verses.toList()
        return books
    }

    private fun getJson(context: Context): String {
        try {
            val inputStream = context.assets.open("books.json")
            val buffer = ByteArray(inputStream.available())
            inputStream.read(buffer)
            inputStream.close()

            return String(buffer, charset("UTF-8"))
        } catch (e: IOException) {
            e.printStackTrace()
            return ""
        }

    }

}
