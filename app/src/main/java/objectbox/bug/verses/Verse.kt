package objectbox.bug.verses

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.annotation.Index
import io.objectbox.relation.ToOne
import objectbox.bug.books.Book

@Entity
class Verse(
        @Id(assignable = true)
        var id: Long,
        @Index
        var bookId: Long,
        @Index
        var chapter: Int,
        var verse: Int,
        var text: String
) {
        //        @TargetIdProperty("bookId")
        lateinit var book: ToOne<Book>
}


