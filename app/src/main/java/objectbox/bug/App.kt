package objectbox.bug

import android.app.Application
import android.support.v7.app.AppCompatActivity
import io.objectbox.BoxStore

class App : Application() {

    val boxStore: BoxStore by lazy {
        MyObjectBox.builder().androidContext(this).build()
    }

    override fun onCreate() {
        super.onCreate()
        app = this
    }

    companion object {
        private var app: App? = null
        fun get(): App {
            return app!!
        }
    }
}


fun AppCompatActivity.boxStore(): BoxStore {
    return (application as App).boxStore
}
